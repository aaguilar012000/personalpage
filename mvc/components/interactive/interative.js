class interactiveCircle extends HTMLElement {
  constructor() {
    super();
  }
  connectedCallback() {
    this.innerHTML = `

    <div class="color-options" id="iterative-menu">
      <h6>Colores</h6>
      <span id="style-color"></span>
      <span id="style-color"></span>
      <span id="style-color"></span>
      <span id="style-color"></span>
      <span  id="style-color"></span>
    </div>

  <div>
    <label>
    <input type="checkbox" id="colors" hidden/>
    <div class="interactive-circle" id="color-page">
      <img class="iterative-icon" src="../icons/palette-solid.svg" alt="">
    </div>
    </label>

    <label>

    <input type="checkbox" id="theme" hidden/>
    <div class="interactive-circle" id="theme-page">
      <img class="iterative-icon" id="moon" src="../icons/moon-solid.svg" alt="">
      <img class="iterative-icon" id="sun" src="../icons/sun-solid.svg" alt="">
    </div>
    </label>
  </div>
	`;

  //cambio de tema
  const circleColor = document.querySelectorAll(".interactive-circle");
  const pageColor = document.getElementById("background-color-1");
  const iconsColor = document.querySelectorAll(".iterative-icon");

  function animatedThemePage() {
    circleColor.forEach((element) => {
      element.classList.toggle("white-color");
    });

    iconsColor.forEach((element) => {
      element.classList.toggle("black-filter");
    });

    if (pageColor.getAttribute("disabled")) {
      pageColor.removeAttribute("disabled");
      localStorage.setItem("themeWhite", "true");
    } else {
      pageColor.setAttribute("disabled", "true");
      localStorage.setItem("themeWhite", "false");
    }
  }

  document
    .getElementById("theme-page")
    .addEventListener("click", animatedThemePage);

  if (localStorage.getItem("themeWhite") === "true") {
    pageColor.removeAttribute("disabled");
  }

  //Fin cambio de tema

  //abrir y cerrar menu de colores//
  document.getElementById("color-page").addEventListener("click", () => {
    document.getElementById("iterative-menu").classList.toggle("slide-r");
  });




  //cambios de color en la pagina

  const colorsOption = document.querySelectorAll("#style-color");


  let color = localStorage.getItem("preferenceColor");
  if(!color){
    localStorage.setItem("preferenceColor", `color-1`)
    color = localStorage.getItem("preferenceColor");
    document.getElementById(color).removeAttribute("disabled");
  }

  if(color){
    document.getElementById(color).removeAttribute("disabled");
  }

  colorsOption.forEach((element, id) => {
    element.addEventListener("click", () => {
      if (color !== `color-${id + 1}`) {
        document.getElementById(color).setAttribute("disabled", "true");
        document
          .getElementById(`color-${id + 1}`)
          .removeAttribute("disabled");
        localStorage.setItem("preferenceColor", `color-${id + 1}`);
        color = localStorage.getItem("preferenceColor");
        document.getElementById("iterative-menu").classList.toggle("slide-r");
      }
    });
  });
  }
}

window.customElements.define("a-interactive", interactiveCircle);
