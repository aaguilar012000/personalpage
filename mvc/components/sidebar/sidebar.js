class sidebar extends HTMLElement {
  constructor() {
    super();
  }
  connectedCallback() {
    this.innerHTML = `
      <label>
        <input type="checkbox" id="check-burger" hidden/>
        <div class="burguer-menu" id="burder-menu">
          <img src="../icons/burger-bars.svg" alt="">
        </div>

			<section class="sidebar" id="sidebar">  
				<h3 class="sidebar_title">AAGUILAR</h3>

				<ul class="sidebar_menu">
					<li>
						<a href="../" id="menu-item-1">
							<span class="menu-item">INICIO</span>
							<img class="icon menu-item" src="../icons/house-solid.svg" />
						</a>
					</li>

					<li>
						<a href="../views/about.html" id="menu-item-2">
							<span class="menu-item">SOBRE MI</span>
							<img class="icon menu-item" src="../icons/user-solid.svg" />
						</a>
					</li>

					<li>
						<a href="../views/portfolio.html" id="menu-item-3">
							<span class="menu-item">PORTFOLIO</span>
							<img class="icon menu-item" src="../icons/folder-open-solid.svg" />
						</a>
					</li>

					<li>
						<a href="../views/contact.html" id="menu-item-4">
							<span class="menu-item">CONTACTO</span>
							<img class="icon menu-item" src="../icons/id-card-solid.svg" />
						</a>
					</li>
				</ul>

        <div class="social-bar">
          <a href="https://gitlab.com/aaguilar012000" target="_blank" class="social-box">
            <img class="icon-social" src="../icons/github-brands.svg"/> 
          </a>

          <a href="https://www.linkedin.com/in/aaguilar012000/" target="_blank" class="social-box">
            <img class="icon-social" src="../icons/linkedin-in-brands.svg"/> 
          </a>

          <a href="https://api.whatsapp.com/send/?phone=%2B584248135166&text&app_absent=0" target="_blank"  class="social-box">
            <img class="icon-social" src="../icons/whatsapp-brands.svg"/> 
          </a>
        </div>
			</section>
      </label>
	`;

    function dectecPage(pathname) {
      switch (pathname){
        case "/":
          document.getElementById('menu-item-1').classList.add('actived')
        break;
        case "/views/about.html":
          document.getElementById('menu-item-2').classList.add('actived')
        break;
        case "/views/portfolio.html":
          document.getElementById('menu-item-3').classList.add('actived')
        break;
        case "/views/contact.html":
          document.getElementById('menu-item-4').classList.add('actived')
        break;
      }
    }

    function animated() {
      document.getElementById("sidebar").classList.add("animated");
      document
        .getElementById("burder-menu")
        .removeEventListener("click", animated);
    }

    document.getElementById("burder-menu").addEventListener("click", animated);

    function actionBarAnimation() {
      const actionBar = document.getElementById("burder-menu");
      const app = document.getElementById("main-content");
      let scrollValue = 150;

      function animation() {
        const scrollAppValue = app?.scrollTop;

        if (scrollAppValue >= scrollValue) {
          actionBar.classList.add("position-action-bar-animated");
        } else {
          actionBar.classList.remove("position-action-bar-animated");
        }

        scrollValue = scrollAppValue;
      }

      if (actionBar !== null) {
        app?.addEventListener("scroll", animation);
      }
    }
    const path = document.location.pathname;
    dectecPage(path);
    actionBarAnimation();
  }
}

window.customElements.define("a-sidebar", sidebar);
