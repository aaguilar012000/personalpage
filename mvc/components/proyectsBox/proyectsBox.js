class proyectsBox extends HTMLElement {
  constructor() {
    super();
    this.image;
    this.page;
  }

  static get observedAttributes() {
    return ['image','page'];
  }

  attributeChangedCallback(nameAtr, oldValue, newValue) {
    switch (nameAtr) {
      case "image":
        this.image = newValue;
        break;

      case "page":
        this.page = newValue;
        break;
    }
  }

  connectedCallback() {
    this.innerHTML = `
    <div class="relative-container">
      <a href="${this.page}" target="_blank">
        <img class="proyect_item" src="${this.image}" alt="proyecto personal">
      </a>
    </div>
    `;
  }
}

window.customElements.define("a-proyect-box", proyectsBox);
