class experienceBox extends HTMLElement {
  constructor() {
    super();

    this.label;
    this.description;
    this.name;
  }

  static get observedAttributes() {
    return ["label", "description","type"];
  }

  attributeChangedCallback(nameAtr, oldValue, newValue) {
    switch (nameAtr) {
      case "type":
        this.name= newValue === '1'? 'Estudios': 'Trabajo';
        break;
      case "label":
        this.label = newValue;
        break;
      case "description":
        this.description = newValue;
        break;
    }
  }

  connectedCallback(){
    this.innerHTML = `
    <div class="experiencie-box">
      <h6 class="experience_label">
        <span>${this.name}</span>${this.label}
      </h6>

      <p class="experiencie_text">
        ${this.description}
      </p>
    </div>
    `
  }
}

window.customElements.define("a-experience-box",experienceBox)
