class progressBar extends HTMLElement {
  constructor() {
    super();
    this.label;
    this.progress;
  }

  static get observedAttributes () {
    return ["label", "progress"];
  }

  attributeChangedCallback(nameAtr, oldValue, newValue) {
    switch (nameAtr) {
      case "label":
        this.label = newValue;
        break;

      case "progress":
        this.progress = newValue;
        break;
    }
  }

  connectedCallback() {
    this.innerHTML = `
      <div>
        <h4 class="progress-bar_label">${this.label}</h4>

        <div  class="progress-bar_bar">
          <div id="progress-bar" class="progress-bar_effect">${this.progress}%</div>
        </div>
      </div>
    `;
    this.querySelector('#progress-bar').setAttribute('data-class', `${this.progress}`);
  }
}

window.customElements.define("a-progress-bar", progressBar);
